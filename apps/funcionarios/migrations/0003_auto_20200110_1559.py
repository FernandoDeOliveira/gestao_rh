# Generated by Django 2.2.9 on 2020-01-10 15:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0002_auto_20200110_1549'),
    ]

    operations = [
        migrations.AlterField(
            model_name='funcionario',
            name='User',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
